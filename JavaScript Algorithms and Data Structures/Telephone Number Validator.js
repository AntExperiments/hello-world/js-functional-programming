function telephoneCheck(str) {
  return /^([1 ]{1,2})?(\(([0-9]{3})\)|[0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(str);
}

telephoneCheck("555-555-5555");